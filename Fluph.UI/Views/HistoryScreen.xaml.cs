﻿using Fluph.UI.Models;
using Fluph.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fluph.UI.Views
{
    /// <summary>
    /// Interaction logic for HistoryScreen.xaml
    /// </summary>
    public partial class HistoryScreen : UserControl
    {
        public HistoryScreen()
        {
            InitializeComponent();
            Loaded += (DataContext as ViewModelBase).Loaded;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fluph.UI.Views
{
    /// <summary>
    /// Interaction logic for AccountsScreen.xaml
    /// </summary>
    public partial class  NumberPad : UserControl
    {


        public NumberPad()
        {
            InitializeComponent();
            Loaded += NumPadLoaded;
        }

        private void NumPadLoaded(object sender, RoutedEventArgs e)
        {
            Input = "";
        }

        public string Input
        {
            get
            {
                return (string) GetValue(InputProperty);
            }
            set
            {
                SetValue(InputProperty, value);
            }
        }

        public ICommand Foo { get { return (ICommand)GetValue(CommandProperty); } set { SetValue(CommandProperty, value); } }

        public Boolean OKButtonEnabled { get { return (Boolean)GetValue(OKButtonEnabledProperty); }
            set
            {
                SetValue(OKButtonEnabledProperty, value);
            } }

        public static readonly DependencyProperty InputProperty = DependencyProperty.Register("Input", typeof(string), typeof(NumberPad));
        public static readonly DependencyProperty OKButtonEnabledProperty = DependencyProperty.Register("OKButtonEnabled", typeof(Boolean), typeof(NumberPad));
        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Foo", typeof(ICommand), typeof(NumberPad));

        //public RoutedCommand OkButtonClickCommand;


        private void NumberButtonClicked(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Input += button.Content;
            Console.WriteLine(Input);
        }

        private void BackButtonClicked(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (Input != null && Input.Length > 0) Input = Input.Substring(0, Input.Length - 1);
        }

        private void OKButtonClicked(object sender, RoutedEventArgs e)
        {
            //if (this.OkButtonClick != null)
            //{
            //    OkButtonClick(this, e);

            //}
        }

        public void EnableOKButton()
        {
            OKButton.IsEnabled = true;
            OKButton.Opacity = 1;
        }

        public void DisableOKButton()
        {
            OKButton.IsEnabled = false;
            OKButton.Opacity = 0.5;
        }
    }
}

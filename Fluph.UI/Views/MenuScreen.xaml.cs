﻿using Fluph.UI.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Fluph.UI.Views
{
    /// <summary>
    /// Interaction logic for MenuScreen.xaml
    /// </summary>
    public partial class MenuScreen : UserControl
    {
        public MenuScreen()
        {
            InitializeComponent();

            Loaded += LoadEvent;

        }

        private void LoadEvent(object sender, RoutedEventArgs e)
        {
            (DataContext as MenuScreenVM).Loaded();
        }
    }
}

﻿using Fluph.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fluph.UI.Views
{
    /// <summary>
    /// Interaction logic for AccountsScreen.xaml
    /// </summary>
    public partial class AccountsScreen : UserControl
    {
        public AccountsScreen()
        {
            InitializeComponent();
        }

        private void lbEurInsuredType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == null)
                return;
            var item = (sender as ListView).SelectedItem;
            (DataContext as AccountsScreenVM).ItemSelected.Execute(item);
        }

        private void lbEurInsuredType_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
    }
}

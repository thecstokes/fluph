﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class MenuScreenVM : ViewModelBase
    {
        public string Balance { get; set; }

        public string AccountName { get; set; }
     
        public ICommand NavToWithdraw { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.WithdrawView));

        public ICommand NavToDeposit { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.DepositView));
        
        public ICommand NavToHistory { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.HistoryView));

        public ICommand NavToTransfer { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.TransferView));

        public ICommand PerviousPageCommand { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.AccountsView));

        internal void Loaded()
        {
            Balance = UserDataBase.ActiveAccount.AccountBalance.ToString() + " " + UserDataBase.ActiveAccount.Denomination.ToString();
            AccountName = UserDataBase.ActiveAccount.Name;
        }

    }
}

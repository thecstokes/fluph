﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class PINInputScreenVM : ViewModelBase
    {
        public ICommand OkButtonCommand { get; } 
            = new RelayCommand(action => {
                Console.WriteLine("Command is firing");
                PageManager.SwitchPage(UIPages.AccountsView);
            });


        public PINInputScreenVM()
        {
            OkButtonCommand = new RelayCommand(async action => {
                Console.WriteLine("Command is firing");
                if (UserDataBase.ActiveAccount.AccountPin.Equals(Input))
                {
                    InvalidInput = false;
                    UserDataBase.CurrentUser = UserDataBase.UserDB.FirstOrDefault(u => u.Accounts.Any(a => a.AccountNumber.Equals(UserDataBase.ActiveAccount.AccountNumber)));
                    //UserDataBase.ActiveUserAccounts = UserDataBase.CurrentUser.Accounts;
                    PageManager.SwitchPage(UIPages.AccountsView);
                }
                else
                {
                    Input = "";
                    InvalidInput = true;
                    await Task.Run(() => System.Threading.Thread.Sleep(3000));
                    InvalidInput = false;
                }

            });
        }

        public bool InvalidInput { get; set; } = false;

        private string input = "";
        public string Input
        {
            get
            {
                return input;
            }
            set
            {
                if (input != value && value.Length <= 4)
                {
                    input = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Input"));
                }

            }
        }

        public Boolean isEntryValid
        {
            get
            {
                return input != null && input.Length == 4;
            }
        }

    }
}

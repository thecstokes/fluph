﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Models;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class AccountsScreenVM : ViewModelBase
    {

        public ICommand ItemSelected = new RelayCommand(action => { UserDataBase.ActiveAccount = action as Account; PageManager.SwitchPage(UIPages.MenuView); });
    }
}

﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Models;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class HistoryScreenVM : ViewModelBase
    {

        public ICommand PerviousPageCommand { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.MenuView));

        public ObservableCollection<Transaction> History { get; private set; } = new ObservableCollection<Transaction>();

        public HistoryScreenVM()
        {
            OnLoad += () =>
             {
                 History.Clear();
                 UserDataBase.ActiveAccount.TransactionHistory.ForEach(trans => History.Add(trans));
             };
        }


    }
}

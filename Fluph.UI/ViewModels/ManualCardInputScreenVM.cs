﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class ManualCardInputScreenVM : ViewModelBase
    {
        public ICommand OkButtonCommand { get; } 

        public ManualCardInputScreenVM()
        {
            OkButtonCommand = new RelayCommand(action => {
                Console.WriteLine("Command is firing");

                UserDataBase.ActiveAccount = UserDataBase.ActiveAccount = UserDataBase.UserDB.SelectMany(u => u.Accounts).FirstOrDefault(a => a.AccountNumber.Equals(Input));
                PageManager.SwitchPage(UIPages.PINInputView);
            });
        }

        private string[] text_boxes = new string[4];
       
        public string[] Boxes
        {
            get
            {
                return text_boxes;
            }
        }

        public Boolean isEntryValid
        {
            get
            {
                return input != null && input.Length == 16;
            }
        }



        private string input = "";
        public string Input
        {
            get
            {
                return input;
            }
            set
            {
                if (input != value && value.Length <= 16)
                {
                    input = value;
                    UpdateTextBoxes();
                    OnPropertyChanged(new PropertyChangedEventArgs("Boxes"));
                    OnPropertyChanged(new PropertyChangedEventArgs("isEntryValid"));
                }
            }
        }

        

        private void UpdateTextBoxes()
        {
            text_boxes = new string[4];
            for (int i = 0; i < input.Length; i++)
            {
                text_boxes[i / 4] += input[i];
            }
        }
        
        
    }
}

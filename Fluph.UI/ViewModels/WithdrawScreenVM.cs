﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class WithdrawScreenVM : ViewModelBase
    {
        public ICommand PerviousPageCommand { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.MenuView));

        public ICommand OkCommand { get; }


        public WithdrawScreenVM()
        {
            OkCommand = new RelayCommand(async action => 
            {
                int amount;
                if (!int.TryParse(Input, out amount))
                    return;
                UserDataBase.ActiveAccount.AccountBalance -= amount;
                UserDataBase.ActiveAccount.TransactionHistory.Add(new Models.Transaction() { Amount = amount, Date = DateTime.Now, Type = Models.TransactionType.Withdraw });
                PageManager.SwitchPage(UIPages.ActionSuccessView);
                await Task.Run(() => System.Threading.Thread.Sleep(3000));
                PageManager.SwitchPage(UIPages.MenuView);

            });
        }
        

        public bool OKButtonEnabled { get { return Input.Length > 0 && UserDataBase.ActiveAccount.AccountBalance >= int.Parse(Input); } }

        public string Input
        {
            get; set;
        } = "";
    }
}

﻿using Fluph.UI.Commands;
using Fluph.UI.Nav;
using System.ComponentModel;
using System.Windows.Input;
using System;
using System.Windows;

namespace Fluph.UI.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand LogoutCommand { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.LoginView));

        public delegate void VoidDelagate();
        public VoidDelagate OnLoad;

        public void Loaded(object sender, RoutedEventArgs e)
        {
            OnLoad();
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
    }
}

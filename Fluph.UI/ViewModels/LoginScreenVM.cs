﻿using Fluph.UI.Commands;
using System.Windows.Input;
using System.Security;
using Fluph.UI.Nav;
using System.Linq;
using Fluph.UI.Constants;

namespace Fluph.UI.ViewModels
{
    public class LoginScreenVM : ViewModelBase
    {
        public ICommand ManualCardSelectCommand { get; } = 
            new RelayCommand(action => 
            {
                PageManager.SwitchPage(UIPages.ManualCardInputView);
            });

        public ICommand CardInsertOverrideCommand { get; } =
            new RelayCommand(action => 
            {
                UserDataBase.ActiveAccount = UserDataBase.UserDB.SelectMany(u => u.Accounts).FirstOrDefault(a => a.AccountNumber.Equals("1111222233334444"));
                PageManager.SwitchPage(UIPages.PINInputView);
            });
    }
}

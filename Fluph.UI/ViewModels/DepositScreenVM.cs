﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class DepositScreenVM : ViewModelBase
    {
        public static bool TransactionComplete { get; set; }

        public static string LastAmount { get; set; }

        public ICommand PerviousPageCommand { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.MenuView));

        public ICommand DepositChequeSim { get; } = new RelayCommand(async action =>
        {
            double amount = 123.45;
            UserDataBase.ActiveAccount.AccountBalance += amount;
            UserDataBase.ActiveAccount.TransactionHistory.Add(new Models.Transaction() {Amount= amount, Date= DateTime.Now, Type=Models.TransactionType.Deposit });
            PageManager.SwitchPage(UIPages.ActionSuccessView);
            await Task.Run(() => System.Threading.Thread.Sleep(3000));
            PageManager.SwitchPage(UIPages.MenuView);
            //await Task.Run(() => DepositRunner(amount));

        }
        );

        public ICommand DepositCashSim { get; } = new RelayCommand(async action =>
        {
            double amount = 21.00;
            UserDataBase.ActiveAccount.AccountBalance += amount;
            UserDataBase.ActiveAccount.TransactionHistory.Add(new Models.Transaction() { Amount = amount, Date = DateTime.Now, Type = Models.TransactionType.Deposit });
            PageManager.SwitchPage(UIPages.ActionSuccessView);
            await Task.Run(() => System.Threading.Thread.Sleep(3000));
            PageManager.SwitchPage(UIPages.MenuView);
            //await Task.Run(() => DepositRunner(amount));

        }
        );

        private static void DepositRunner(double amount)
        {
            LastAmount = amount.ToString();
            System.Threading.Thread.Sleep(200);

            TransactionComplete = true;

            System.Threading.Thread.Sleep(1000);
            PageManager.SwitchPage(UIPages.MenuView);
            TransactionComplete = false;
        }

    }
}

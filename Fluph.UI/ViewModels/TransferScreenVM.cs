﻿using Fluph.UI.Commands;
using Fluph.UI.Constants;
using Fluph.UI.Models;
using Fluph.UI.Nav;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fluph.UI.ViewModels
{
    public class TransferScreenVM : ViewModelBase
    {
        public ICommand PerviousPageCommand { get; } = new RelayCommand(action => PageManager.SwitchPage(UIPages.MenuView));
        public ICommand OkButtonCommand { get; }
            = new RelayCommand(action => {
                Console.WriteLine("Command is firing");

                PageManager.SwitchPage(UIPages.AccountsView);
            });



        public ICommand OKButtonCommand { get; }

        public Account From { get; set; }

        public Account To { get; set; }


        public TransferScreenVM()
        {
            OKButtonCommand = new RelayCommand(async action => 
            {
                Console.WriteLine("From Account {0}", From.AccountNumber);

                int amount = int.Parse(Input);
                From.AccountBalance -= amount;
                To.AccountBalance += amount;
                From.TransactionHistory.Add(new Transaction() { Amount = amount, Date = DateTime.Now, Type = TransactionType.Transfer });
                To.TransactionHistory.Add(new Transaction() { Amount = amount, Date = DateTime.Now, Type = TransactionType.Deposit });
                PageManager.SwitchPage(UIPages.ActionSuccessView);
                await Task.Run(() => System.Threading.Thread.Sleep(3000));
                PageManager.SwitchPage(UIPages.MenuView);
            });
        }

        public void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private string input = "";
        public string Input
        {
            get
            {
                return input;
            }
            set
            {
                if (input != value && value.Length <= 4)
                {
                    input = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Input"));
                }

            }
        }

        public Boolean isEntryValid
        {
            get
            {
                return input != null && input.Length >= 1 && To != null && From != null;
            }
        }
    }
}

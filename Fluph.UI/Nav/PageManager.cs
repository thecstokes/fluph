﻿namespace Fluph.UI.Nav
{
    public static class PageManager
    {
        internal static MainWindow FluphWindow { private get; set; }

        public static void SwitchPage(UIPages newPage)
        {
            FluphWindow.Navigate(newPage);
        }
    }
}

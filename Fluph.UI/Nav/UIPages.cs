﻿using Fluph.UI.Views;
using System.Windows.Controls;

namespace Fluph.UI.Nav
{
    public class UIPages
    {
        public UserControl page { get; }

        public static readonly UIPages LoginView = new UIPages(new LoginScreen());
        public static readonly UIPages ManualCardInputView = new UIPages(new ManualCardInputScreen());
        public static readonly UIPages PINInputView = new UIPages(new PINInputScreen());
        public static readonly UIPages MenuView = new UIPages(new MenuScreen());
        public static readonly UIPages DepositView = new UIPages(new DepositScreen());
        public static readonly UIPages HistoryView = new UIPages(new HistoryScreen());
        public static readonly UIPages TransferView = new UIPages(new TransferScreen());
        public static readonly UIPages WithdrawView = new UIPages(new WithdrawScreen());
        public static readonly UIPages AccountsView = new UIPages(new AccountsScreen());
        public static readonly UIPages ActionSuccessView = new UIPages(new ActionSuccess());



        private UIPages(UserControl page)
        {
            this.page = page;
        }
    }
}

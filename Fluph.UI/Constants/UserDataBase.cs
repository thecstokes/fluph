﻿using Fluph.UI.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Fluph.UI.Constants
{
    public static class UserDataBase
    {

        private static User _CurrentUser;

        public static User CurrentUser
        {
            get { return _CurrentUser; }
            set { _CurrentUser = value; RefreshAccounts(value.Accounts); }
        }

        private static void RefreshAccounts(ObservableCollection<Account> accounts)
        {
            ActiveUserAccounts.Clear();
            foreach(Account a in accounts)
            {
                ActiveUserAccounts.Add(a);
            }
        }

        public static ObservableCollection<Account> ActiveUserAccounts { get; set; } = new ObservableCollection<Account>();





        public static Account ActiveAccount { get; set; }

        public static ObservableCollection<User> UserDB = new ObservableCollection<User>() {
            new User()
            {
                Name ="Barry Scholastic",
                Accounts = new ObservableCollection<Account>
                {
                    new Account()
                    {
                        AccountBalance = 11000.00,
                        AccountNumber = "1111222233334444",
                        AccountPin = "1234",
                        Name = "Pizza Money",
                        Denomination = Currency.BTC,
                        TransactionHistory = new List<Transaction>() { new Transaction() { Amount=11000.00, Date=DateTime.Parse("May-22-2010"), Type=TransactionType.Deposit } }
                    },
                    new Account()
                    {
                        AccountBalance = 1000.00,
                        AccountNumber = "1111111111111111",
                        Name = "Family Checking",
                        AccountPin = "1111",
                        Denomination = Currency.CAD,
                        TransactionHistory = new List<Transaction>() { new Transaction() { Amount=11000.00, Date=DateTime.Parse("May-22-2010"), Type=TransactionType.Deposit } }
                    }
                }
            },
            new User()
            {
                Name = "Anker McBankface",
                Accounts = new ObservableCollection<Account>()
                {
                    new Account()
                    {
                        AccountBalance = 0.01,
                        AccountNumber = "9999999999999999",
                        AccountPin = "9999",
                        Name = "Everyday Checking",
                        Denomination = Currency.CAD,
                        TransactionHistory = new List<Transaction>()
                        {
                            new Transaction() { Amount=2000.00, Date=DateTime.Parse("March-10-2017"), Type=TransactionType.Deposit },
                            new Transaction() { Amount = 1999.99, Date = DateTime.Parse("June-18-2017"), Type = TransactionType.Withdraw }
                        }
                    },
                    new Account()
                    {
                        AccountBalance = 1000000.00,
                        AccountNumber = "8888888888888888",
                        Name = "Tax Evasion Money",
                        AccountPin = "8888",
                        Denomination = Currency.CAD,
                        TransactionHistory = new List<Transaction>()
                        {
                            new Transaction() { Amount=10000.00, Date=DateTime.Parse("August-22-2016"), Type=TransactionType.Deposit },
                            new Transaction() { Amount=36.00, Date=DateTime.Parse("August-28-2016"), Type=TransactionType.Deposit },
                            new Transaction() { Amount=10036.00, Date=DateTime.Parse("November-12-2017"), Type=TransactionType.Withdraw },
                            new Transaction() { Amount=1000000.00, Date=DateTime.Parse("November-13-2017"), Type=TransactionType.Deposit }
                        }
                    },
                    new Account()
                    {
                        AccountBalance = 10.00,
                        AccountNumber = "7777777777777777",
                        Name = "Lucky Charms",
                        AccountPin = "7777",
                        Denomination = Currency.BTC,
                        TransactionHistory = new List<Transaction>() {
                            new Transaction() { Amount=11000.00, Date=DateTime.Parse("May-22-2010"), Type=TransactionType.Deposit},
                            new Transaction() { Amount=11000.00, Date=DateTime.Parse("May-22-2010"), Type=TransactionType.Transfer, To= "1111222233334444" },
                            new Transaction() { Amount=1, Date=DateTime.Parse("March-22-2012"), Type=TransactionType.Deposit},
                            new Transaction() { Amount=9, Date=DateTime.Parse("July-01-2014"), Type=TransactionType.Deposit},
                        }
                    }
                }
            }

        };

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluph.UI.Models
{
    public class User
    {
        public ObservableCollection<Account> Accounts { get; set; }

        public string Name { get; set; }

    }
}

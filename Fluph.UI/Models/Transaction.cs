﻿using System;

namespace Fluph.UI.Models
{
    public enum TransactionType { Transfer, Withdraw, Deposit }

    public class Transaction
    {
        public double Amount { get; set; }

        public TransactionType Type { get; set; }

        /// <summary>
        /// Where its going, only for Transfer
        /// </summary>
        public string To { get; set; }

        public DateTime Date { get; set; }

    }
}
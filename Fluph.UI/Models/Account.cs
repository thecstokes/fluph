﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluph.UI.Models
{
    public class Account
    {
        public string AccountNumber { get; set;  }

        public string Name { get; set;  }

        public string AccountPin { get; set;  }

        public double AccountBalance { get; set; }

        public Currency Denomination { get; set;  }

        public List<Transaction> TransactionHistory { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}

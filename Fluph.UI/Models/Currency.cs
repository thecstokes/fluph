﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluph.UI.Models
{
    public enum Currency
    {
        CAD, BTC
    }
    

    public static class CurrencyConverter
    {

        private static readonly double BTCtoCADRate = 10500.00;

        public static double CurrencyConversionRate(this Currency curr, Currency to)
        {
            if (curr.Equals(to))
            {
                return 1.0;
            }
            else
            {
                if(curr == Currency.CAD)
                {
                    return 1 / BTCtoCADRate;
                }
                else
                {
                    return BTCtoCADRate;
                }
            }

        }
    }
}
﻿using Fluph.UI.Nav;
using System;
using System.Windows.Input;

namespace Fluph.UI.Commands
{
    class SwitchPageCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object page)
        {
            if(page is UIPages)
            {
                PageManager.SwitchPage((UIPages)page);
            }
            else
            {
                throw new InvalidCastException("Specified object was not a UIPages object, cannot navigate to it!");
            }
        }
    }
}
